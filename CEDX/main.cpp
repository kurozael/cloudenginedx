#include "Graphics/Window.h"
#include "Graphics/Renderer.h"

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int iCmdShow)
{
	cedx::Window* window = new cedx::Window(hInstance, iCmdShow);
	window->Register(L"MyGame");

	cedx::Renderer* renderer = new cedx::Renderer(window);
	renderer->Create();

	while (true)
	{
		MSG msg = window->Update();

		if(msg.message == WM_QUIT)
			break;

		renderer->GetDevice()->Clear(0, NULL, D3DCLEAR_TARGET, D3DCOLOR_XRGB(0, 40, 100), 1.0f, 0);
		renderer->GetDevice()->BeginScene();
		renderer->GetDevice()->EndScene();
		renderer->GetDevice()->Present(NULL, NULL, NULL, NULL);
	}
}