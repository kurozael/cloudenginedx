/*
	� 2011 CloudSixteen.com do not share, re-distribute or modify
	this file without the permission of its owner(s).
*/

#include "Window.h"

namespace cedx
{
	LRESULT CALLBACK Window::WindowProc(HWND wndHandle, UINT message, WPARAM wParam, LPARAM lParam)
	{
		switch(message)
		{
			case WM_DESTROY:
			{
				PostQuitMessage(0);
				return 0;
			}

			break;
		}

		return DefWindowProc(wndHandle, message, wParam, lParam);
	}

	HWND& Window::GetHandle()
	{
		return m_wndHandle;
	}

	void Window::Register(const std::wstring& className)
	{
		m_wndClass.lpszClassName = className.c_str();
		RegisterClassEx(&m_wndClass);

		m_wndHandle = CreateWindowEx(NULL, className.c_str(), L"CEDX_Engine",
			WS_OVERLAPPEDWINDOW,
			300,    // x-position of the window
			300,    // y-position of the window
			500,    // width of the window
			400,    // height of the window
			NULL,    // we have no parent window, NULL
			NULL,    // we aren't using menus, NULL
			m_appInstance,    // application handle
			NULL
		);

		ShowWindow(m_wndHandle, m_iCmdShow);
	}

	MSG& Window::Update()
	{
		while(PeekMessage(&m_msgHandle, NULL, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&m_msgHandle);
			DispatchMessage(&m_msgHandle);
		}

		return m_msgHandle;
	}

	Window::Window(HINSTANCE appInstance, int iCmdShow)
	{
		ZeroMemory(&m_wndClass, sizeof(m_wndClass));

		m_appInstance = appInstance;
		m_iCmdShow = iCmdShow;
		m_wndClass.cbSize = sizeof(m_wndClass);
		m_wndClass.style = CS_HREDRAW | CS_VREDRAW;
		m_wndClass.lpfnWndProc = &Window::WindowProc;
		m_wndClass.hInstance = appInstance;
		m_wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
		m_wndClass.hbrBackground = (HBRUSH)COLOR_WINDOW;
	}

	Window::~Window(void)
	{
		DestroyWindow(m_wndHandle);
	}
}
