/*
	� 2011 CloudSixteen.com do not share, re-distribute or modify
	this file without the permission of its owner(s).
*/

#pragma once
#pragma comment (lib, "d3d9.lib")

#include "Window.h"
#include <d3d9.h>

namespace cedx
{
	class Renderer
	{
	public:
		LPDIRECT3DDEVICE9& GetDevice();
		void Create(void);
	public:
		Renderer(Window* window);
		~Renderer(void);
	private:
		D3DPRESENT_PARAMETERS m_dxParams;
		LPDIRECT3DDEVICE9 m_dxDev;
		LPDIRECT3D9 m_dx3D;
		Window* m_window;
	};
}