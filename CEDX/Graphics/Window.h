/*
	� 2011 CloudSixteen.com do not share, re-distribute or modify
	this file without the permission of its owner(s).
*/

#pragma once

#include <windows.h>
#include <windowsx.h>
#include <string>

namespace cedx
{
	class Window
	{
	protected:
		static LRESULT CALLBACK WindowProc(HWND wndHandle, UINT message, WPARAM wParam, LPARAM lParam);
	public:
		void Register(const std::wstring& className);
		HWND& GetHandle();
		MSG& Update();
	public:
		Window(HINSTANCE m_appInstance, int iCmdShow);
		~Window(void);
	private:
		WNDCLASSEX m_wndClass;
		HINSTANCE m_appInstance;
		HWND m_wndHandle;
		MSG m_msgHandle;
		int m_iCmdShow;
	};
}

