/*
	� 2011 CloudSixteen.com do not share, re-distribute or modify
	this file without the permission of its owner(s).
*/

#include <Color.h>

namespace cedx
{
	void Color::SetAlpha(float na)
	{
		a = na;
	}

	void Color::Normalize()
	{
		r /= 255.0f;
		g /= 255.0f;
		b /= 255.0f;
		a /= 255.0f;
	}

	void Color::ToWhite()
	{
		r = 255.0f;
		g = 255.0f;
		b = 255.0f;
	}

	void Color::ToBlack()
	{
		r = 0.0f;
		g = 0.0f;
		b = 0.0f;
	}

	Color::Color(float nr, float ng, float nb, float na)
	{
		r = nr; g = ng;
		b = nb; a = na;
	}
	
	Color::Color() : r(255.0f), g(255.0f), b(255.0f), a(255.0f) {}
	
	Color::~Color() {}
}