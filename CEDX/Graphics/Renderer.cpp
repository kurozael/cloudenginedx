/*
	� 2011 CloudSixteen.com do not share, re-distribute or modify
	this file without the permission of its owner(s).
*/

#include "Renderer.h"

namespace cedx
{
	LPDIRECT3DDEVICE9& Renderer::GetDevice()
	{
		return m_dxDev;
	}

	void Renderer::Create(void)
	{
		m_dx3D->CreateDevice(
			D3DADAPTER_DEFAULT,
			D3DDEVTYPE_HAL,
			m_window->GetHandle(),
			D3DCREATE_SOFTWARE_VERTEXPROCESSING,
			&m_dxParams,
			&m_dxDev
		);
	}

	Renderer::Renderer(Window* window)
	{
		m_dx3D = Direct3DCreate9(D3D_SDK_VERSION);
		m_window = window;
		ZeroMemory(&m_dxParams, sizeof(m_dxParams));

		m_dxParams.Windowed = TRUE;
		m_dxParams.SwapEffect = D3DSWAPEFFECT_DISCARD;
		m_dxParams.hDeviceWindow = m_window->GetHandle();
	}


	Renderer::~Renderer(void)
	{
		m_dxDev->Release();
		m_dx3D->Release();
	}
}